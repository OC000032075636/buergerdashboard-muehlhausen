# Bürgerdashboard Mühlhausen


## Name
Bürgerdashboard Mühlhausen

## Beschreibung
Im Rahmen des Smart City Projekts wurde bei der Stadtverwaltung Mühlhausen der Aufbau einer Datenplattform im Sinne einer Sammlung, Verarbeitung und intelligenten Vernetzung von Daten aus verschiedenen Quellen sowie die Verarbeitung und Visualisierung von Daten und Echtzeitdaten auf einem Dashboard realisiert. Die Anwendung soll dazu beitragen, dass Daten für Bürger und Unternehmen frei zur Verfügung gestellt werden (Open Data).

## Installation
Das Projekt ist auf einer verteilten Hardware installiert. Das Backend zur Sammlung der Daten ist in einer Windows Umgebung mit einen MS SQL Server lauffähig, das öffentliche Frontend ist für einen Linux-Server konzipiert.
Die Komponenten sind auf Basis von Pentaho Data Integraton für das Backend und Pentaho BI-Server für das Frontend entwickelt. In diesem OpenCoDE Repository sind alle für das Projekt entwickelten Komponenten eingecheckt und damit die zugrundeliegenden Ideen öffentlich. Es ist keine "out of the box" installierbare Umgebung eingecheckt, was aufgrund der indivuellen Gestaltung der Schnittstellen, die sich exakt auf den Bedarf der Stadt Mühlhausen beziehen, nicht sinnvoll ist.

## Nutzung
Eine sinnvolle Nutzung der hier veröfentlichten Source könnte sein, einzelne Komponenten, etwa die Abbildung der Wetterdaten herauszunehmen und die Aufbereitung der Meldungen des Deutschen Wetterdienstes sowie die finale Abbildung in einem Pentaho Frontend für andere Dashboards zu übernehmen. Ebenso kann die Aufbereitung der Daten von Cleverciti oder Hystreet übernommen werden, wenn diese Anbieter auf für andere Nutzer Daten bereitstellen.

## Support
Bei Fragen wenden Sie Sich bitte an den Projekteigentümer, aktuell Herrn Freytag, den  Fachdienstleiter Smart City der Stadtverwaltung Mühlhausen 

## Roadmap
Das Projekt ist im ersten Release im Januar 2024 abgeschlossen worden. Erweiterungen sind angedacht, aber noch nicht projektiert.

## Contributing
Zum aktuellen Status ist die Mitarbeit aus der OpenSource Community nicht vorgesehen, aber das Projekt "Bürgerdashboard Mühlhausen" ist für Anmerkungen sowie aktive Teilnahme offen und würde es begrüßen, wenn es diesbezüglich Kommentare oder Anfragen geben würde.

## Authors and acknowledgment
Das Projekt ist eine Zusammenarbeit der Stadt Mühlhausen mit dem Anbieter PASS IT Consulting, Aschaffenburg. Allen Mitwirkenden wird für die gute und vertrauensvolle Zusammenarbeit hiermit gedankt.

## License
Der für das Projekt erstellte Code wird unter die MIT-Lizenz gestellt, hier der Lizenztext:

Copyright (c) 2024 Stadt Mühlhausen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Phase 1 des Projektes ist abgeschlossen, das Dashboard ist im Produktivbetrieb.
