USE [MH_Cube]
GO
/****** Object:  Table [Output_Layer].[o_hystreet]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Output_Layer].[o_hystreet](
	[stichtag_bez] [varchar](10) NULL,
	[stichtag] [date] NULL,
	[Stätte] [int] NULL,
	[Oberer Steinweg] [int] NULL,
	[Unterer Steinweg] [int] NULL,
	[Linsenstraße] [int] NULL
) ON [PRIMARY]
GO
