USE [MH_Cube]
GO
/****** Object:  Table [Input_Layer].[i_clevercity_free]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Input_Layer].[i_clevercity_free](
	[name] [varchar](100) NULL,
	[departureTime] [datetime2](7) NULL,
	[sysTime] [datetime2](7) NULL
) ON [PRIMARY]
GO
