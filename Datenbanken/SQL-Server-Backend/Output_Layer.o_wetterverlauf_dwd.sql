USE [MH_Cube]
GO
/****** Object:  Table [Output_Layer].[o_wetterverlauf_dwd]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Output_Layer].[o_wetterverlauf_dwd](
	[ttt_min] [int] NULL,
	[ttt_max] [int] NULL,
	[wettersymbol] [varchar](100) NULL,
	[Wochentag] [varchar](3) NULL,
	[tag] [varchar](10) NULL
) ON [PRIMARY]
GO
