USE [MH_Cube]
GO
/****** Object:  Table [Kettle].[kettle_job_log]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Kettle].[kettle_job_log](
	[ID_JOB] [int] NULL,
	[CHANNEL_ID] [varchar](255) NULL,
	[JOBNAME] [varchar](255) NULL,
	[STATUS] [varchar](15) NULL,
	[LINES_READ] [bigint] NULL,
	[LINES_WRITTEN] [bigint] NULL,
	[LINES_UPDATED] [bigint] NULL,
	[LINES_INPUT] [bigint] NULL,
	[LINES_OUTPUT] [bigint] NULL,
	[LINES_REJECTED] [bigint] NULL,
	[ERRORS] [bigint] NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[LOGDATE] [datetime] NULL,
	[DEPDATE] [datetime] NULL,
	[REPLAYDATE] [datetime] NULL,
	[LOG_FIELD] [text] NULL,
	[EXECUTING_SERVER] [varchar](255) NULL,
	[EXECUTING_USER] [varchar](255) NULL,
	[START_JOB_ENTRY] [varchar](255) NULL,
	[CLIENT] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
