USE [MH_Cube]
GO
/****** Object:  Table [Output_Layer].[o_temperaturverlauf]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Output_Layer].[o_temperaturverlauf](
	[uhrzeit] [int] NULL,
	[temperatur] [decimal](6, 2) NULL
) ON [PRIMARY]
GO
