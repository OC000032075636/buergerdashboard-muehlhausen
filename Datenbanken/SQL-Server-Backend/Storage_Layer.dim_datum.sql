USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[dim_datum]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[dim_datum](
	[dat_id] [int] NOT NULL,
	[datum] [date] NULL,
	[datum_string] [varchar](10) NULL,
	[datum_jahr] [int] NULL,
	[datum_quartal] [varchar](20) NULL,
	[datum_monat] [varchar](20) NULL,
	[datum_tag] [varchar](20) NULL,
	[datum_monat_solo] [int] NULL,
	[datum_tag_solo] [int] NULL,
	[datum_tag_der_woche] [smallint] NULL,
	[datum_wochentag] [varchar](10) NULL,
	[datum_wochentag_kalender] [varchar](2) NULL,
	[datum_wochentag_kalender_mo] [varchar](3) NULL,
	[datum_ist_monatsultimo] [varchar](5) NULL,
	[datum_ist_jahresultimo] [varchar](5) NULL
) ON [PRIMARY]
GO
