USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_DWD_MetElementDefinition]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_DWD_MetElementDefinition](
	[ShortName] [varchar](10) NULL,
	[UnitOfMeasurement] [varchar](20) NULL,
	[description] [varchar](200) NULL
) ON [PRIMARY]
GO
