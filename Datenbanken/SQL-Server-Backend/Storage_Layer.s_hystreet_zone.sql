USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_hystreet_zone]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_hystreet_zone](
	[id] [int] NULL,
	[Standort] [varchar](100) NULL,
	[Zone_ID] [int] NULL,
	[Stadt] [varchar](100) NULL,
	[Bundesland] [varchar](100) NULL,
	[Messungen_Start] [datetime] NULL,
	[Messungen_Ende] [datetime] NULL,
	[Messungen_erste] [datetime] NULL,
	[Messungen_letzte] [datetime] NULL,
	[Richtung_links] [varchar](100) NULL,
	[Richtung_rechts] [varchar](100) NULL,
	[Zeitstempel] [datetime] NULL,
	[Wetterbedingung] [varchar](100) NULL,
	[Temperatur] [decimal](10, 2) NULL,
	[Zone_Fussgaenger_Anzahl] [bigint] NULL,
	[Zone_Fussgaenger_Anzahl_links] [bigint] NULL,
	[Zone_Fussgaenger_Anzahl_rechts] [bigint] NULL,
	[Zone_Fussgaenger_Anzahl_Erwachsen] [bigint] NULL,
	[Zone_Fussgaenger_Anzahl_Kind] [bigint] NULL,
	[Incident_ID] [int] NULL,
	[Incident_Name] [varchar](100) NULL,
	[Incident_Logo] [varchar](100) NULL,
	[Incident_Beschreibung] [varchar](8000) NULL,
	[Incident_von] [datetime] NULL,
	[Incident_bis] [datetime] NULL
) ON [PRIMARY]
GO
