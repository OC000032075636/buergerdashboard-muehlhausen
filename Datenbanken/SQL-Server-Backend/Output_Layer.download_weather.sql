USE [MH_Cube]
GO
/****** Object:  View [Output_Layer].[download_weather]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Skript für SelectTopNRows-Befehl aus SSMS ******/
create view [Output_Layer].[download_weather] as
SELECT tag Datum
      ,TTT_min [Temperatur Min]
      ,TTT_max [Tempratur Max]
      ,n_min [Bewölkung % Min]
      ,n_max [Bewölkung %_Max]
      ,RR1c_max [Niederschlag je Stunde Max]
      ,wwT_max [Gewitterwahrscheinlichkeit %]
      ,SunD1_max [Sonnenschein je Stunde Max]
      ,count_SunD1_600 [Stunden mit mind 10 Minuten Sonne]
      ,FF_max [Windstärke m/s Max]
      ,FX1_max [Windböen m/s Max]
      ,ww_min [Wettercode Min]
      ,ww_max [Wettercode Max]
      ,wettersymbol Wettersymbol
  FROM MH_Cube.Storage_Layer.s_Wetteranalyse_DWD
GO
