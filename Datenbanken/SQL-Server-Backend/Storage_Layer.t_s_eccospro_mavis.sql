USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[t_s_eccospro_mavis]    Script Date: 31.01.2024 18:20:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[t_s_eccospro_mavis](
	[zeitpunkt] [datetime2](7) NULL,
	[Anzahl_Zugang_Tagessumme] [int] NULL,
	[Anzahl_aktuell_Summe] [int] NULL,
	[Anzahl_aktuell_Therme] [int] NULL,
	[Anzahl_aktuell_Sauna] [int] NULL,
	[Anzahl_Zugang_Freibad] [int] NULL
) ON [PRIMARY]
GO
