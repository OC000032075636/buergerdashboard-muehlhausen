USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[DWD_Definition]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[DWD_Definition](
	[Abkuerzung] [varchar](100) NULL,
	[Bedeutung] [varchar](3000) NULL
) ON [PRIMARY]
GO
