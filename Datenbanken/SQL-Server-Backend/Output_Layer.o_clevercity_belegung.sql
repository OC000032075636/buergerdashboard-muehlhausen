USE [MH_Cube]
GO
/****** Object:  Table [Output_Layer].[o_clevercity_belegung]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Output_Layer].[o_clevercity_belegung](
	[typ] [varchar](9) NULL,
	[Status] [varchar](6) NULL,
	[anzahl] [int] NULL
) ON [PRIMARY]
GO
