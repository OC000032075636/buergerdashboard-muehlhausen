USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_hystreet_standort_tag]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_hystreet_standort_tag](
	[id] [int] NULL,
	[Standort] [varchar](100) NULL,
	[Stadt] [varchar](100) NULL,
	[Bundesland] [varchar](100) NULL,
	[Richtung_links] [varchar](100) NULL,
	[Richtung_rechts] [varchar](100) NULL,
	[Stichtag] [date] NULL,
	[Temperatur_max] [decimal](10, 2) NULL,
	[Temperatur_min] [decimal](10, 2) NULL,
	[Temperatur_avg] [decimal](5, 2) NULL,
	[Fussgaenger_Anzahl] [bigint] NULL,
	[Fussgaenger_Anzahl_links] [bigint] NULL,
	[Fussgaenger_Anzahl_rechts] [bigint] NULL,
	[Fussgaenger_Anzahl_Erwachsen] [bigint] NULL,
	[Fussgaenger_Anzahl_Kinder] [bigint] NULL,
	[Fussgaenger_Anzahl_Erwachsen_links] [bigint] NULL,
	[Fussgaenger_Anzahl_Erwachsen_rechts] [bigint] NULL,
	[Fussgaenger_Anzahl_Kind_links] [bigint] NULL,
	[Fussgaenger_Anzahl_Kind_rechts] [bigint] NULL
) ON [PRIMARY]
GO
