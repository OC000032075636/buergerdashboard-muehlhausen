USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_mhl_log]    Script Date: 31.01.2024 18:20:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_mhl_log](
	[ip_address_hash] [varchar](100) NULL,
	[event_date] [date] NULL,
	[event_time] [varchar](20) NULL,
	[path] [varchar](100) NULL,
	[returncode] [int] NULL,
	[return_length] [int] NULL
) ON [PRIMARY]
GO
