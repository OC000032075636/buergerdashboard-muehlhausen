USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_mhl_kalender_total]    Script Date: 31.01.2024 18:20:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_mhl_kalender_total](
	[UID] [varchar](50) NULL,
	[EVENT_NUM] [int] NULL,
	[CATEGORIES] [varchar](200) NULL,
	[SUMMARY] [varchar](200) NULL,
	[DESCRIPTION] [varchar](1000) NULL,
	[STATUS] [varchar](50) NULL,
	[DTSTART_TZID_Europe_Berlin] [datetime] NULL,
	[DTEND_TZID_Europe_Berlin] [datetime] NULL,
	[DTSTART_TZID_Europe_Berlin_DATE] [varchar](10) NULL,
	[DTEND_TZID_Europe_Berlin_DATE] [varchar](10) NULL,
	[LOCATION] [varchar](500) NULL,
	[GEO] [varchar](50) NULL,
	[ORGANIZER] [varchar](200) NULL,
	[PRIORITY] [int] NULL,
	[SEQUENCE] [int] NULL,
	[URL] [varchar](500) NULL,
	[RRULE] [varchar](50) NULL,
	[DTSTAMP] [datetime] NULL,
	[rrule_frequence] [varchar](100) NULL,
	[rrule_until] [datetime] NULL,
	[rrule_byday] [varchar](100) NULL,
	[satzart] [varchar](100) NULL
) ON [PRIMARY]
GO
