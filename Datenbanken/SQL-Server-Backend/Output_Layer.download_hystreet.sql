USE [MH_Cube]
GO
/****** Object:  View [Output_Layer].[download_hystreet]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Skript für SelectTopNRows-Befehl aus SSMS ******/
create view [Output_Layer].[download_hystreet] as
SELECT DISTINCT [Messungen_Start]
      ,[Messungen_Ende]
	  ,[Standort]
      ,[Fussgaenger_Anzahl]
      ,[Fussgaenger_Anzahl_links]
      ,[Fussgaenger_Anzahl_rechts]
      ,[Fussgaenger_Anzahl_Erwachsen]
      ,[Fussgaenger_Anzahl_Kinder]
  FROM [MH_Cube].[Storage_Layer].[s_hystreet_standort]
--  order by id, Messungen_Start desc
GO
