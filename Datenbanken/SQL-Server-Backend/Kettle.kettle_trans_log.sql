USE [MH_Cube]
GO
/****** Object:  Table [Kettle].[kettle_trans_log]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Kettle].[kettle_trans_log](
	[ID_BATCH] [int] NULL,
	[CHANNEL_ID] [varchar](255) NULL,
	[TRANSNAME] [varchar](255) NULL,
	[STATUS] [varchar](15) NULL,
	[LINES_READ] [bigint] NULL,
	[LINES_WRITTEN] [bigint] NULL,
	[LINES_UPDATED] [bigint] NULL,
	[LINES_INPUT] [bigint] NULL,
	[LINES_OUTPUT] [bigint] NULL,
	[LINES_REJECTED] [bigint] NULL,
	[ERRORS] [bigint] NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[LOGDATE] [datetime] NULL,
	[DEPDATE] [datetime] NULL,
	[REPLAYDATE] [datetime] NULL,
	[LOG_FIELD] [text] NULL,
	[EXECUTING_SERVER] [varchar](255) NULL,
	[EXECUTING_USER] [varchar](255) NULL,
	[CLIENT] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
