USE [MH_Cube]
GO
/****** Object:  Table [Input_Layer].[i_Hystreetdaten]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Input_Layer].[i_Hystreetdaten](
	[id] [int] NULL,
	[name] [varchar](100) NULL,
	[city] [varchar](100) NULL,
	[population] [int] NULL,
	[state] [varchar](100) NULL,
	[today_count] [int] NULL,
	[timerange_count] [int] NULL,
	[measured_from] [datetime] NULL,
	[measured_to] [datetime] NULL,
	[earliest_measurement_at] [datetime] NULL,
	[latest_measurement_at] [datetime] NULL,
	[ltr_label] [varchar](100) NULL,
	[rtl_label] [varchar](100) NULL,
	[timestamp] [datetime] NULL,
	[weather_condition] [varchar](100) NULL,
	[temperature] [decimal](10, 2) NULL,
	[min_temperature] [decimal](10, 2) NULL,
	[pedestrians_count] [bigint] NULL,
	[unverified] [varchar](100) NULL,
	[ltr_pedestrians_count] [bigint] NULL,
	[rtl_pedestrians_count] [bigint] NULL,
	[adult_pedestrians_count] [bigint] NULL,
	[child_pedestrians_count] [bigint] NULL,
	[adult_ltr_pedestrians_count] [bigint] NULL,
	[adult_rtl_pedestrians_count] [bigint] NULL,
	[child_ltr_pedestrians_count] [bigint] NULL,
	[child_rtl_pedestrians_count] [bigint] NULL,
	[zone_id] [int] NULL,
	[zone_pedestrians_count] [bigint] NULL,
	[zone_ltr_pedestrians_count] [bigint] NULL,
	[zone_rtl_pedestrians_count] [bigint] NULL,
	[zone_adult_pedestrians_count] [bigint] NULL,
	[zone_child_pedestrians_count] [bigint] NULL,
	[Incident_id] [int] NULL,
	[Incident_Name] [varchar](100) NULL,
	[Incident_Icon] [varchar](100) NULL,
	[Incident_decription] [varchar](8000) NULL,
	[Incident_von] [datetime] NULL,
	[Incident_bis] [datetime] NULL
) ON [PRIMARY]
GO
