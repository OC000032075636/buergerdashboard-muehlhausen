USE [MH_Cube]
GO
/****** Object:  View [Output_Layer].[hystreet_zone]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [Output_Layer].[hystreet_zone] as
select *
from Storage_Layer.s_hystreet_zone
GO
