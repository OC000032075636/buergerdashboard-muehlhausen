USE [MH_Cube]
GO
/****** Object:  Table [Input_Layer].[i_mhl_kalender]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Input_Layer].[i_mhl_kalender](
	[EVENT_NUM] [int] NULL,
	[CATEGORIES] [varchar](200) NULL,
	[SUMMARY] [varchar](200) NULL,
	[DESCRIPTION] [varchar](1000) NULL,
	[STATUS] [varchar](50) NULL,
	[DTSTART_TZID_Europe_Berlin] [datetime] NULL,
	[DTEND_TZID_Europe_Berlin] [datetime] NULL,
	[DTSTART_TZID_Europe_Berlin_DATE] [date] NULL,
	[DTEND_TZID_Europe_Berlin_DATE] [date] NULL,
	[LOCATION] [varchar](500) NULL,
	[GEO] [varchar](50) NULL,
	[ORGANIZER] [varchar](200) NULL,
	[PRIORITY] [int] NULL,
	[SEQUENCE] [int] NULL,
	[UID] [varchar](50) NULL,
	[URL] [varchar](500) NULL,
	[RRULE] [varchar](50) NULL,
	[DTSTAMP] [datetime] NULL
) ON [PRIMARY]
GO
