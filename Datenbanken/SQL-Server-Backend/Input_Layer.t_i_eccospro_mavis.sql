USE [MH_Cube]
GO
/****** Object:  Table [Input_Layer].[t_i_eccospro_mavis]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Input_Layer].[t_i_eccospro_mavis](
	[zeitpunkt] [datetime2](7) NULL,
	[overall] [int] NULL,
	[count_sum] [int] NULL,
	[sectorNr] [int] NULL,
	[sectorName] [varchar](100) NULL,
	[count] [int] NULL
) ON [PRIMARY]
GO
