USE [MH_Cube]
GO
/****** Object:  Table [Storage_Layer].[s_Wetteranalyse_DWD]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Storage_Layer].[s_Wetteranalyse_DWD](
	[tag] [date] NULL,
	[TTT_min] [numeric](6, 2) NULL,
	[TTT_max] [numeric](6, 2) NULL,
	[n_min] [decimal](5, 2) NULL,
	[n_max] [decimal](5, 2) NULL,
	[RR1c_max] [decimal](8, 2) NULL,
	[RRS1c_max] [decimal](8, 2) NULL,
	[wwT_max] [decimal](8, 2) NULL,
	[SunD1_max] [decimal](8, 2) NULL,
	[count_SunD1_600] [int] NULL,
	[FF_max] [decimal](5, 2) NULL,
	[FX1_max] [decimal](5, 2) NULL,
	[ww_min] [decimal](5, 2) NULL,
	[ww_max] [decimal](5, 2) NULL,
	[wettersymbol] [varchar](100) NULL
) ON [PRIMARY]
GO
