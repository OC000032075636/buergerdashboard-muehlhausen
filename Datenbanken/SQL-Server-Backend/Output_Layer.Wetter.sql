USE [MH_Cube]
GO
/****** Object:  View [Output_Layer].[Wetter]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Skript für SelectTopNRows-Befehl aus SSMS ******/
create view [Output_Layer].[Wetter] as
SELECT [Datum]
      ,[Uhrzeit]
      ,[Temperatur]
      ,[Taupunkt]
      ,[Feuchtigkeit]
      ,[Niederschlag]
      ,[Schnee]
      ,[Windrichtung]
      ,[Windgeschwindigkeit]
      ,[Windstoss]
      ,[Luftdruck]
      ,[Sonnenschein]
      ,[Wetterkondition]
  
  FROM [MH_Cube].[Input_Layer].[i_Wetterdaten]
  where Temperatur is not null
GO
