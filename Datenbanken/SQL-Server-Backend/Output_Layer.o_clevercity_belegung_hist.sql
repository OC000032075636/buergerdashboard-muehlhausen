USE [MH_Cube]
GO
/****** Object:  Table [Output_Layer].[o_clevercity_belegung_hist]    Script Date: 31.01.2024 18:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Output_Layer].[o_clevercity_belegung_hist](
	[Datum] [date] NULL,
	[Uhrzeit] [varchar](5) NULL,
	[PKW_belegt] [int] NULL,
	[PKW_frei] [int] NULL,
	[Wohnmobil_belegt] [int] NULL,
	[Wohnmobil_frei] [int] NULL,
	[Barrierefrei_belegt] [int] NULL,
	[Barrierefrei_frei] [int] NULL,
	[Elektro_belegt] [int] NULL,
	[Elektro_frei] [int] NULL
) ON [PRIMARY]
GO
