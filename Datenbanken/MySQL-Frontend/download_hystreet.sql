CREATE TABLE `download_hystreet` (
  `Messungen_Start` datetime DEFAULT NULL,
  `Messungen_Ende` datetime DEFAULT NULL,
  `Standort` varchar(100) DEFAULT NULL,
  `Fussgaenger_Anzahl` int(11) DEFAULT NULL,
  `Fussgaenger_Anzahl_links` int(11) DEFAULT NULL,
  `Fussgaenger_Anzahl_rechts` int(11) DEFAULT NULL,
  `Fussgaenger_Anzahl_Erwachsen` int(11) DEFAULT NULL,
  `Fussgaenger_Anzahl_Kinder` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
