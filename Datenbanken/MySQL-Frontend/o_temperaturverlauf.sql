CREATE TABLE `o_temperaturverlauf` (
  `uhrzeit` int(11) DEFAULT NULL,
  `temperatur` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
