CREATE TABLE `o_clevercity_belegung_hist` (
  `Datum` date DEFAULT NULL,
  `Uhrzeit` varchar(5) DEFAULT NULL,
  `PKW_belegt` int(11) DEFAULT NULL,
  `PKW_frei` int(11) DEFAULT NULL,
  `Wohnmobil_belegt` int(11) DEFAULT NULL,
  `Wohnmobil_frei` int(11) DEFAULT NULL,
  `Barrierefrei_belegt` int(11) DEFAULT NULL,
  `Barrierefrei_frei` int(11) DEFAULT NULL,
  `Elektro_belegt` int(11) DEFAULT NULL,
  `Elektro_frei` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
