CREATE TABLE `o_hystreet` (
  `Stichtag_Bez` varchar(9) DEFAULT NULL,
  `stichtag` date DEFAULT NULL,
  `Stätte` int(11) DEFAULT NULL,
  `Oberer Steinweg` int(11) DEFAULT NULL,
  `Unterer Steinweg` int(11) DEFAULT NULL,
  `Linsenstraße` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
