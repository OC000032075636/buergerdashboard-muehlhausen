CREATE VIEW download_weather AS
select
`mhl_cube`.`s_wetteranalyse_dwd`.`tag` AS `Datum`,
`mhl_cube`.`s_wetteranalyse_dwd`.`TTT_min` AS `Temperatur Min`,
`mhl_cube`.`s_wetteranalyse_dwd`.`TTT_max` AS `Tempratur Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`n_min` AS `Bewölkung % Min`,
`mhl_cube`.`s_wetteranalyse_dwd`.`n_max` AS `Bewölkung %_Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`RR1c_max` AS `Niederschlag je Stunde Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`wwT_max` AS `Gewitterwahrscheinlichkeit %`,
`mhl_cube`.`s_wetteranalyse_dwd`.`SunD1_max` AS `Sonnenschein je Stunde Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`count_SunD1_600` AS `Stunden mit mind 10 Minuten Sonne`,
`mhl_cube`.`s_wetteranalyse_dwd`.`FF_max` AS `Windstärke m/s Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`FX1_max` AS `Windböen m/s Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`ww_min` AS `Wettercode Min`,
`mhl_cube`.`s_wetteranalyse_dwd`.`ww_max` AS `Wettercode Max`,
`mhl_cube`.`s_wetteranalyse_dwd`.`wettersymbol` AS `Wettersymbol`
from `mhl_cube`.`s_wetteranalyse_dwd` ;