CREATE TABLE `s_wetteranalyse_dwd` (
  `tag` varchar(10) DEFAULT NULL,
  `TTT_min` decimal(8,2) DEFAULT NULL,
  `TTT_max` decimal(8,2) DEFAULT NULL,
  `n_min` decimal(8,2) DEFAULT NULL,
  `n_max` decimal(8,2) DEFAULT NULL,
  `RR1c_max` decimal(8,2) DEFAULT NULL,
  `wwT_max` decimal(8,2) DEFAULT NULL,
  `SunD1_max` decimal(8,2) DEFAULT NULL,
  `count_SunD1_600` int(11) DEFAULT NULL,
  `FF_max` decimal(8,2) DEFAULT NULL,
  `FX1_max` decimal(8,2) DEFAULT NULL,
  `ww_min` decimal(8,2) DEFAULT NULL,
  `ww_max` decimal(8,2) DEFAULT NULL,
  `wettersymbol` varchar(100) DEFAULT NULL,
  `RRS1c_max` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
