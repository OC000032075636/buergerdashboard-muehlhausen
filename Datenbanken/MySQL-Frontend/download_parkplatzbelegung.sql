CREATE VIEW download_parkplatzbelegung AS
select
`mhl_cube`.`o_clevercity_belegung_hist`.`Datum` AS `Datum`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Uhrzeit` AS `Uhrzeit`,
`mhl_cube`.`o_clevercity_belegung_hist`.`PKW_belegt` AS `PKW_belegt`,
`mhl_cube`.`o_clevercity_belegung_hist`.`PKW_frei` AS `PKW_frei`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Wohnmobil_belegt` AS `Wohnmobil_belegt`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Wohnmobil_frei` AS `Wohnmobil_frei`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Barrierefrei_belegt` AS `Barrierefrei_belegt`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Barrierefrei_frei` AS `Barrierefrei_frei`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Elektro_belegt` AS `Elektro_belegt`,
`mhl_cube`.`o_clevercity_belegung_hist`.`Elektro_frei` AS `Elektro_frei`
from `mhl_cube`.`o_clevercity_belegung_hist`
where 1 ;
