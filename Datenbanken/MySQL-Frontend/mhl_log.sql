CREATE TABLE `mhl_log` (
  `ip_address_hash` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(20) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `returncode` int(11) DEFAULT NULL,
  `return_length` int(11) DEFAULT NULL,
  KEY `ix1_mhl_log` (`ip_address_hash`,`event_date`,`event_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
