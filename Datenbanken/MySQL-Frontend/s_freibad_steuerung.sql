CREATE TABLE `s_freibad_steuerung` (
  `response` varchar(2000) DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `zustand` varchar(20) DEFAULT NULL,
  `zeit_ab` varchar(10) DEFAULT NULL,
  `zeit_bis` varchar(10) DEFAULT NULL,
  `insertTimestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
