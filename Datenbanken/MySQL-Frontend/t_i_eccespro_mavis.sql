CREATE TABLE `t_i_eccospro_mavis` (
  `zeitpunkt` datetime DEFAULT NULL,
  `overall` int(11) DEFAULT NULL,
  `count_sum` int(11) DEFAULT NULL,
  `sectorNr` int(11) DEFAULT NULL,
  `sectorName` tinytext DEFAULT NULL,
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ;
