# Bürgerdashboard Mühlhausen

## Verzeichnisinhalt Dashboard-Backend

[de] Dieses Direktory enthält die Komponenten zur Datensammlung und Datenaufbereitung des Bürgerdashboards der Stadt Mühlhausen/Thüringen. Es handelt sich um Transformationen und Jobs zu Pentaho Dataintegration 9.4, die in einer neuen Pentaho DI Umgebung prinzipiell lauffähig sind. Nicht enthalten sind die Konfigurationen, die REST Token, User-/Passwort Informationen und nicht öffentliche Links enthalten. Somit ist die komplette Logik abgelegt, aber eine Neuinstallation bedarf einiger Konfiguration, um eine lauffähige Umgebung zu erstellen.

Zusätzlich muss die Backend-Datenbank installiert werden, die in diesem Fall aufgrund der Vorgaben der Stadt Mühlhausen auf dem Microsoft SQL Server beruht, die entsprechenden Sourcen sind in einem parallelen Verzeichnis eingestellt. Eine Migration auf eine OpenSouce Datenbank ist ohne weiteres möglich.
