@echo off

cd C:\Pentaho\pdi-ce-9.4.0.0-343\data-integration

call Kitchen.bat /file:"C:\Pentaho\MH-Cube_ETL\Jobs\a_import_60.kjb"  "-param:cleverciti_userid=xxx" "-param:cleverciti_groupid=xxx" "-param:cleverciti_api_key=xxx" > C:\Pentaho\MH-Cube_ETL\Logs\a_import_60.log 2> C:\Pentaho\MH-Cube_ETL\Logs\a_import_60.error
