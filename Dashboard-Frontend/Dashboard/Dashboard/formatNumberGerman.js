function formatNumber(value1, scale) {
    value1 = value1.toFixed(scale);
    var value2 = "";
    if (!isNaN(parseFloat(value1)) && isFinite(value1)){
        value2 = addCommas(value1)
    }
    else {
        value2 = value1 + "";
    }
    return value2;   

}

function addCommas(value1) {
    value1 += "";
    x = value1.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
