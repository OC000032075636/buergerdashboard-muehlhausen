var var_header       = "header_2col";
var var_wetter       = "wetter_2col";
var var_info_therme  = "info_therme_2col";
var var_parkplatz    = "parkplatz_2col";
var var_passanten    = "passanten_2col";
var var_termine      = "termine_2col";
var var_links        = "links_2col";
var var_download     = "download_2col";

if (window.innerWidth < 1200)
{
    var_header       = "header_1col";
    var_wetter       = "wetter_1col";
    var_info_therme  = "info_therme_1col";
    var_parkplatz    = "parkplatz_1col";
    var_passanten    = "passanten_1col";
    var_termine      = "termine_1col";
    var_links        = "links_1col";
    var_download     = "download_1col";
}


require([
  "dash!/public/Dashboard/Dashboard_Header3.wcdf"
], function(header) {
  (new header(var_header)).render();
});

require([
  "dash!/public/Dashboard/Wetterverlauf_3.wcdf"
], function(wetter) {
  (new wetter(var_wetter)).render();
});

require([
  "dash!/public/Dashboard/Infobereich_Therme_3.wcdf"
], function(info_therme) {
  (new info_therme(var_info_therme)).render();
});

require([
  "dash!/public/Dashboard/Parkplatz_Hanfsack3.wcdf"
], function(parkplatz) {
  (new parkplatz(var_parkplatz)).render();
});

require([
  "dash!/public/Dashboard/Passanten_woechentlich3.wcdf"
], function(passanten) {
  (new passanten(var_passanten)).render();
});

require([
  "dash!/public/Dashboard/Infobereich_Termine_3.wcdf"
], function(info_termine) {
  (new info_termine(var_termine)).render();
});

require([
  "dash!/public/Dashboard/Infobereich_Links_3.wcdf"
], function(info_links) {
  (new info_links(var_links)).render();
});

require([
  "dash!/public/Dashboard/Infobereich_Downloads_3.wcdf"
], function(info_download) {
  (new info_download(var_download)).render();
});

