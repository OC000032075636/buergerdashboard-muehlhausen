# Bürgerdashboard Mühlhausen

## Verzeichnisinhalt Dashboard-Frontend

### Deutsch

Dieses Verzeichnis enthält die für das Frontend des Bürgerdashboards der Stadt Mühlhausen/Thüringen entwickelten Komponenten. Das Frontend ist unter dem Link https://dashboard.muehlhausen.de öffentlich zugänglich.

Die Frontendkomponenten des Bürgerdashboards der Stadt Mühlhausen sind in eine Ablaufumgebung von Pentaho BI-Server 9.4 eingebunden. Eingecheckt ist der komplette virtuelle Pfad des BI-Servers mit allen Files und den Export-Metadaten (exportManifest.xml). Damit kann das Frontend an anderer Stelle wieder in eine Pentaho BI-Server 9.x Umgebung (oder höher) eingespielt werden. Lauffähig ist es für sich alleine nicht, weil in diesem Verzeichnis nicht die Tabellenstruktur der dazugehörenden MySQL Datenbank abgelegt ist. Dies ist in einem parallelen Verzeichnis der Fall.

### English

This directory contains all components developed for the frontend of "Bürgerdashboard der Stadt Mühlhausen/Thüringen". It can be accessed under the link https://dashboard.muehlhausen.de and is open to the public.

The frontend components are running in a Pentaho BI-Server 9.4 environment. All files of the virtual path in the BI-Server are checked in together with metadata information (exportManifest.xml). It can be deployed in another Pentaho BI-Server 9.x environment (or higher). Without the accompanying database (MySQL) it cannot be run. You find the definition of the database in a parallel directory.
